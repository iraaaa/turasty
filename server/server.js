import express from 'express';
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import cors from 'cors';

import { internalConfig, logger, auth0 } from './config/';
import { schema, rootValue, context } from './data/index';

const { PORT } = internalConfig;
const app = express();
app.use(cors());

app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

app.use('/graphql', bodyParser.json(), graphqlExpress(request => ({
  schema,
  rootValue,
  context: context(request.headers, auth0, internalConfig),
})));

app.listen(PORT, () => logger.log('info', `API server running on localhost: ${PORT}`));
