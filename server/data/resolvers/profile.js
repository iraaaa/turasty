export default {
  Query: {
    selfProfile: async (root, args, context) => {
      const session = context.driver.session();
      // context will have user information from auth0
      const user = await context.user;
      const query = `MATCH (p:Profile {username:"${user.username}"}) RETURN p`;
      return session.run(query, args).then(result => result.records.map(record => record.get('p').properties)[0]);
    },
    getProfile: (root, args, context) => {
      console.log("get profile is called");
      const session = context.driver.session();
      console.log("args received", args.username);
      // context will have user information from auth0
      const query = `MATCH (p:Profile {username:"${args.username}"}) RETURN p`;
      return session.run(query, args).then((result) => {
        console.log("here us the result of query", result);
        return result.records.map(record => record.get('p').properties)[0];
      });
    },
  },
  Mutation: {
    createNewProfile: async (root, args, context) => {
      console.log("create new profile is called");
      const session = context.driver.session();
      console.log("args received", args.username);
      // context will have user information from auth0
      const user = await context.user;
      const query = `CREATE (p:Profile {username:"${user.name}",avatar:"${user.picture}",bio:"${args.bio}", countries:0, published:0}) RETURN p`;
      return session
              .run(query, args)
              .then((result) => {
                return result.records.map(record => record.get('p').properties)[0];
              })
              .catch(error => console.log(error));
    },
  },
  Profile: {
    subscribers: (root, args, context) => {
      const session = context.driver.session();
      const query = `MATCH (p:Profile {username:"${args.username}"})<-[:SUBSCRIBED_TO]-(s:Profile) RETURN s`;
      return session.run(query, args).then(result => result.records.map(record => record.get('s').properties));
    },
    subscribedTo: (root, args, context) => {
      const session = context.driver.session();
      const query = `MATCH (p:Profile {username:"${args.username}"})-[:SUBSCRIBED_TO]->(s:Profile) RETURN s`;
      return session.run(query, args).then(result => result.records.map(record => record.get('s').properties));
    },
  },
};
