// import uuid from 'uuid';

export default {
  Query: {
    tripsByUsername: (root, args, context) => {
      const session = context.driver.session();
      const query = `MATCH (p:Profile {username:"${args.username}"})-[r:IS_AUTHOR_OF]->(t:Trip) RETURN t`;
      return session.run(query, args).then(result => result.records.map(record => record.get('t').properties)[0]);
    },
  },
  Mutation: {
    createNewTrip: async (root, args, context) => {
      const session = context.driver.session();
      // context will have user information from auth0
      const user = await context.user;
      let trip = args.trip;
      // console.log(JSON.stringify(trip, null, '\t'));
      let storiesQ = '';
      trip.stories.forEach((story, sindex) => {
        storiesQ += `CREATE (t)-[:HAS_STORIES]->(s${sindex}:Story {storyID:"${sindex}", title:"${story.title}", tags:"${story.tags}"})`;
        let slQ = '';
        story.locations.forEach((storyLocation, index) => {
          const picture = storyLocation.picture || 'https://via.placeholder.com/150';
          slQ += `CREATE (sl${index}:StoryLocation {order:${index}, picture:"${picture}", rate:"${storyLocation.rate}"})-[:BELONGS_TO]->(s${sindex}) 
          CREATE (sl${index})-[:HAS]->(l${index}:Location {locationID:"l${index}", name:"${storyLocation.name}"}) `;
        });
        storiesQ += slQ;
      });

      let query = `MATCH (p:Profile {username: "${user.name}"})
      CREATE (p)-[:IS_AUTHOR_OF]->(t:Trip {tripID:"${trip.tripID}", title:"${trip.tripTitle}", cover:"${trip.cover}"})
      CREATE (t)-[:HAS_BUDGET]->(b:Budget {value:"${trip.budget.value}", currency:"${trip.budget.currency}"})
      CREATE (t)-[:HAS_DURATION]->(d:Duration {value:"${trip.duration.value}", unit:"${trip.duration.unit}"})
      `;
      query += storiesQ;
      query += ";";
      // console.log(query);
      return session.run(query, args).then((result) => {
        console.log(result);
        // hack because some bug causes neo4j to return null instead of returning mutation result.
        if (result.records.length == 0) {
          console.log("here");
          query = `MATCH (p:Profile {username:"${user.name}"})-[r:IS_AUTHOR_OF]->(t:Trip {tripID:"${trip.tripID}"}) RETURN t`;
          return session.run(query, args).then(res => res.records.map(rec => rec.get('t').properties)[0]).then(console.log);
        } else {
          return result.records.map(record => record.get('p').properties)[0];
        }
        
      });
    },
  },

};
