import { v1 as neo4j } from 'neo4j-driver';
import path from 'path';
import jwt from 'jsonwebtoken';
import { makeExecutableSchema } from 'graphql-tools';
import { fileLoader, mergeTypes, mergeResolvers } from 'merge-graphql-schemas';

const typeDefs = mergeTypes(fileLoader(path.join(__dirname, './schemas')));
const resolvers = mergeResolvers(fileLoader(path.join(__dirname, './resolvers')));

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

let driver;
export function context(headers, auth0, secrets) {
  // context will have information which we can user to extract the user token etc
  const token = headers.authorization;
  const user = new Promise((resolve, reject) => {
    jwt.verify(token, auth0.getKey, auth0.options, (err, decoded) => {
      if (err) {
        return reject(err);
      }
      return resolve(decoded);
    });
  });

  if (!driver) {
    driver = neo4j.driver(
      secrets.DB_URI,
      neo4j.auth.basic(secrets.DB_USER, secrets.DB_PWD),
    );
  }
  return { driver, user };
}
