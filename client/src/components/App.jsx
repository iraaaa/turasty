import React, { Component } from 'react';
import Nav from './shared/Nav/Nav.jsx';
import { Switch, Route, withRouter } from 'react-router-dom';

import Home from './pages/Home/Home.jsx';
import Profile from './pages/Profile/SelfProfile.jsx';
import NewUser from './pages/Profile/NewUser.jsx';
import Create from './pages/CreateTrip/Create.jsx';
import Callback from './Callback';
import auth from '../Auth';
import GuardedRoute from './GuardedRoute';

class App extends Component {

  async componentDidMount() {
    if (this.props.location.pathname === '/callback') return;
    try {
      await auth.silentAuth();
      this.forceUpdate();
    } catch (err) {
      if (err.error === 'login_required') return;
      console.log(err.error);
    }
  }

  render() {
    return (
      <div className="App">
        <Nav />
        <main>
          {this.props.children}
          <Switch>
            <Route exact path='/' component={Home}/>
            <GuardedRoute path='/profile' component={Profile}/>
            <GuardedRoute path='/newUser' component={NewUser}/>
            <GuardedRoute path='/create' component={Create}/>
            <Route path='/callback' component={Callback} />
          </Switch>
        </main>
      </div>
    );
  }
}

export default withRouter(App);