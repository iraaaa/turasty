import React, {Component} from 'react';

import Search from '../../shared/Search/Search.jsx';
import GridTabCombo from '../../shared/Combos/GridTabCombo.jsx';

import {getAllTrips,getHomeTabs} from '../../../data/data.js';

import './Home.css';

export default class Home extends Component{
  render(){
    const tabData = getHomeTabs();
    const tripData= getAllTrips();
    return(
      <div className="Home__container">
        <div className="Home__heading__box">
          <h1 className="Home__heading__box__headline">Turasty</h1>
          <p className="Home__heading__box__subHeadline">
          Word of mouth just got cooler. Get inspired. Travel more. Plan less.
          </p>
        </div>
        <div className="Home__Search__box">
          <Search />
        </div>
        <div className="Home__data__box">
          <GridTabCombo tripData={tripData} tabData={tabData} />
        </div>
      </div>
    )
  }
}
