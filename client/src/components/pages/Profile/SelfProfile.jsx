import React, {Component} from 'react';
import { gql } from "apollo-boost";

import auth from '../../../Auth';
import getClient from '../../../Client';
import GridTabCombo from '../../shared/Combos/GridTabCombo.jsx';
import {getProfileTabs} from '../../../data/data.js';

// import {userData,getProfileTabs,getAllTrips} from '../../../data/data.js';

import './Profile.css';

function getProfileQ(username) {
  return gql`
    query getProfile {
      getProfile(username:"${username}"){
        username,
        avatar,
        bio,
        countries,
        published,
        subscribers {
          username,
        },
        subscribedTo {
          username,
        },
      }
    }
  `;
}

function getTripQ(username) {
  return gql`
    query tripsByUsername {
      tripsByUsername(username:"${username}"){
        author {
          bio,
          countries,
          published,
        },
        cover,
        title,
        tags,
        stories
      }
    }
  `;
}

export default class SelfProfile extends Component{

  constructor(props){
    super(props);
    this.state = {
      data: {},
      trips: [],
    }
  }

  async componentWillMount(){
    this.client  = await getClient();
    const client = this.client;
    await auth.getProfile(async (err, profile) => { 
      if(!err){
        this.setState({username:profile.name});
        const GET_USER = getProfileQ(profile.name);
        //if data is null, query for the user as they might already exist
        client.query({
          query: GET_USER,
        })
        .then((data) => {
          const userData = data.data.getProfile;
          if(userData) {
            this.setState({data: userData});
          }
        });
      }
    });
  }

  async componentDidMount(){
    if(!this.client) {
      await this.componentWillMount();
    }
    const GET_TRIPS = getProfileQ(this.state.username);
    console.log(GET_TRIPS);
    this.client.query({
      query: GET_TRIPS,
    })
    .then((data) => {
      const userTrips = data.data.tripsByUsername;
      console.log("queryTrips:", userTrips);
      if(userTrips) {
        this.setState({trips: userTrips});
      }
    });
  }
  
  render(){
    let tabData = getProfileTabs();
    return (
      <div className="Profile__box">
        <section className="Profile__intro__box">
          <img src={this.state.data && this.state.data.avatar} className="Profile__img" alt="Not available"/>
          <span className="Profile__uname">{`@${this.state.data.username}`}</span>
          <h2 className="Profile__name">
            {this.state.data && this.state.data.username}
          </h2>
        </section>
        <section className="Profile__data">
          { Object.keys(this.state.trips).length == 0 ? 
            (
            <div className="Profile__data__none">
              <p className="Profile__data__none__tagline">You haven't published any trips yet! Share your adventures with the community and help others discover new trips :)</p> 
              <a href="/create" className="Profile__btnGrp__btn Profile__btnGrp__createTripBtn">Create New Trip</a>
            </div>
            )
            : 
            <GridTabCombo tabData={tabData} tripData={this.state.trips} />
          }
          
        </section>
      </div>
    )
  }
}