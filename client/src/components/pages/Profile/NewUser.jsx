import React, {Component} from 'react';
// import { graphql, Query } from "react-apollo";
import { gql } from "apollo-boost";
import getClient from '../../../Client';
// import GridTabCombo from '../../shared/Combos/GridTabCombo.jsx';

// import {userData,getProfileTabs,getAllTrips} from '../../../data/data.js';

import './Profile.css';

function mutationQ(picture){
  return gql`
    mutation createNewProfile{
      createNewProfile(avatar: "${picture}") {
          username
          avatar
        }
      }
    `;
}

function getProfileQ(username) {
  return gql`
    query getProfile {
      getProfile(username:"${username}"){
        username,
        avatar,
        bio,
        countries,
        published,
        subscribers {
          username,
        },
        subscribedTo {
          username,
        },
      }
    }
  `;
}


export default class NewUser extends Component{

  constructor(props){
    super(props);
    this.state = {
      data: {},
    }
  }

  async componentWillMount(){
    console.log("data is ",  this.props.location.state);
    this.client  = await getClient();
    const NEW_USER = mutationQ(this.props.location.state.picture);
    this.client.mutate({
      mutation: NEW_USER,
    })
    .then((data) => {
      const userData = data.data.createNewProfile;
      console.log("mutation:", userData);
      if(!userData) {
        const GET_USER = getProfileQ(this.props.location.state.name);
        //if data is null, query for the user as they might already exist
        this.client.query({
          query: GET_USER,
        })
        .then((data) => {
          const userData = data.data.getProfile;
          console.log("query:", userData);
          if(userData) {
            this.setState({data: userData});
          }
        });
      } else {
        this.setState({data: userData});
      }
    });
  }
  
  render(){
    return (
      <div className="Profile__box">
        <section className="Profile__intro__box">
          <img src={this.state.data && this.state.data.avatar} className="Profile__img" alt="Not available"/>
          <span className="Profile__uname">{`@${this.state.data.username}`}</span>
          <h2 className="Profile__name">
            {this.state.data && this.state.data.username}
          </h2>
        </section>
      </div>
    )
  }
}