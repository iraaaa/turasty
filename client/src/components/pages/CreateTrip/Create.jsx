import React, {Component} from 'react';
// import { Button, Card, Col, Row, Empty, Typography, Icon } from 'antd';
import { BackTop, Select, Input, Row, Col, Button } from 'antd';
import { gql } from "apollo-boost";
import stringifyObject from 'stringify-object';
import './Create.css';

import Upload from '../../shared/Input/File/File.jsx';
import Story from '../../shared/Story/Story-new.jsx';
import coverImg from '../../../data/img/cover.jpg';
import getID from '../../../utils/genID';
import getClient from '../../../Client';
 

function mutationQ(tripData){

  tripData = stringifyObject(tripData, {
    singleQuotes: false
  });
  console.log("what do we have here",`yo "${tripData}"`);  
  console.log("umm", tripData.toString());  
  return gql`
    mutation createNewTrip{
      createNewTrip(trip:${tripData}) {
          tripId
          # author {
          #   username
          # }
          # title
          # cover
          # budget {
          #   value
          #   currency
          # }
          # tags
          # stories {
          #   title
          #   tags
          # }
        }
      }
    `;
}

export default class Create extends Component{
  constructor(props){
    super(props);
    
    this.tripID = getID('trip-');
    this.state = {
      cover:coverImg,
      tripTitle:"",
      budget:{value:0, currency:"USD"},
      duration:{value:1, unit:"week"},
      stories:[],
    }
    this.uploadPicture = this.uploadPicture.bind(this);
    this.publish = this.publish.bind(this);
  }

  updateTripTitle = (e) => {
    this.setState({tripTitle: e.target.value});
  }

  uploadPicture = (e) => {
    // upload to s3 and get back url of the uploaded image
  }

  updateBudgetCurrency = (e) => {
    this.setState({budget: {...this.state.budget, currency: e}});
  }

  updateBudgetValue = (e) => {
    this.setState({budget: {...this.state.budget, value: e.target.value}});
  }

  updateDurationUnit = (e) => {
    this.setState({duration: {...this.state.duration, unit: e}});
  }

  updateDurationValue = (e) => {
    this.setState({duration: {...this.state.duration, value: e.target.value}});
  }

  async publish(e){
    console.log({...this.state, tripID: this.tripID});
    this.client  = await getClient();
    const res = JSON.parse(JSON.stringify({...this.state, tripID: getID('trip-')}));
    console.log(res);
    const NEW_TRIP = mutationQ(res);
    this.client.mutate({
      mutation: NEW_TRIP
    })
    .then((data) => {
      console.log("mutation done");
      console.log("data is", data);
    });

  }

  // addStory(){
  //   const newStory = `story-${this.state.stories.length}`;
  //   // give story-id
  //   // add to locations -> story.locations
  //   // return a story react component with ID set as key with single location
  // }
  // addLocation(storyID){
  //   const newLocation = `loc-${this.state.stories.storyID.locations.length}`;
  //   // give location-<id>
  //   // return a react component with id set as key with single location
  // }

  uploadMediaForLocation(fileList,storyID,locationID){
    console.log("called upload", fileList);
    // const s3Url = this.uploadPicture();
    // update state for location with location_id => location name and 
    // this.state.stories.storyID.locations.locationID.media.push(s3Url);
  }

  handleUpdate = (stories) => {
    console.log("updated data", stories);
    this.setState({stories});
  }
  render(){
    const Option = Select.Option;
    const InputGroup =  Input.Group;
    // this.tripID = getID('trip-');
    return (
      <div className="CreateTrip__box">
        <section className="CreateTrip__publishBtn">
          <Button type="primary" size="large" onClick={this.publish}>Publish</Button>
        </section>
        <section className="CreateTrip__header">
          <input className="CreateTrip__header__input" type="text" placeholder="How would you name this trip?" value={this.state.tripTitle} onChange={this.updateTripTitle} />
          <br/>
          <Row gutter={16}>
            <Col span={12}>
              <InputGroup compact>
                <Select value={this.state.budget.currency} onChange={this.updateBudgetCurrency}>
                  <Option value="USD">USD</Option>
                  <Option value="CAD">CAD</Option>
                  <Option value="INR">INR</Option>
                </Select>
                <Input addonBefore="Trip Budget" type="number" style={{ width: '70%' }} placeholder="Money spent" onChange={this.updateBudgetValue} value={this.state.budget.value} />
              </InputGroup>
            </Col>
            <Col span={12}>
              <InputGroup compact>
                <Select value={this.state.duration.unit} onChange={this.updateDurationUnit}>
                  <Option value="day">Day</Option>
                  <Option value="week">Week</Option>
                  <Option value="month">Month</Option>
                </Select>
                <Input addonBefore="Trip Duration" type="number" style={{ width: '70%' }} placeholder="Time spent" onChange={this.updateDurationValue} value={this.state.duration.value} />
              </InputGroup>
            </Col>
          </Row>
        </section>
        <section className="CreateTrip__cover">
          <img className="CreateTrip__cover__img" src={this.state.cover} alt="" />
          <Upload className="CreateTrip__cover__uploadBtn" handleChangeFn={this.uploadCover} accept="image/*">
            <i className="fa fa-camera-retro CreateTrip__cover__uploadBtn__btn" aria-hidden="true"></i>
            <span className="CreateTrip__cover__uploadBtn__label">
              Add that <span className="CreateTrip__cover__label--big">Showstopper</span> picture.
            </span>
          </Upload>
        </section>
        <section className="CreateTrip__body">
            <Story tripID={this.tripID} handleStoryUpdate={(changedValues,allValues)=> this.handleUpdate(changedValues, allValues)}/>    
        </section>
        <BackTop>
          <div className="ant-back-top-inner ScrollUp-btn"><span>SCROLL UP</span></div>
        </BackTop>
      </div>
    )
  }
}
