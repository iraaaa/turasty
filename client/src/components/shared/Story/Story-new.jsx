import React, {Component} from 'react';
import { Form, Input, Icon, Button, Empty, Select} from 'antd';
import _ from 'lodash';
import StoryLocation from './StoryLocation';
import './Story.css';

const { Option } = Select;
let id=0;
let stories = [];
class Story extends Component {

  remove = (k) => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys,
    });
  }

  // handleSubmit = (e) => {
  //   e.preventDefault();
  //   this.props.form.validateFields((err, values) => {
  //     if (!err) {
  //       console.log('Received values of form: ', values);
  //     }
  //   });
  // }

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { tripID } = this.props;
    getFieldDecorator('keys', { initialValue: [] });
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => (
      <div className="StoryOutline__form" key={`${tripID}-story-${k}`}>
        <Form.Item
          {...formItemLayout}
          required={false}
          key={k}
          className="Story__form"
        >
          <Form.Item label="Title" hasFeedback name={`story-${k}-title`}>
            {getFieldDecorator(`story-${k}-title`, {
              rules: [
                { required: true, message: 'Every story needs a name :(' },
              ],
            })(
              <Input placeholder="Story Name" />
            )}
          </Form.Item>

          <Form.Item label="Tags" name={`story-${k}-tags`}>
            {getFieldDecorator(`story-${k}-tags`, {
              rules: [
                { required: true, message: 'Tags help people create categories of stories easily!', type: 'array' },
              ],
            })(
              <Select mode="multiple" placeholder="Please select some tags that best define your story!">
                <Option value="adventure">Adventure</Option>
                <Option value="relaxing">Relaxing</Option>
                <Option value="hip">Hip</Option>
                <Option value="rare-find">Rare Find</Option>
                <Option value="remote">Remote</Option>
                <Option value="city">City</Option>
                <Option value="romantic">Romantic</Option>
                <Option value="family">Family</Option>
                <Option value="culinary">Culinary</Option>
                <Option value="hiking">Hiking</Option>
              </Select>
            )}
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 0 }} name={`story-${k}-locations`}><StoryLocation storyID={`story-${k}`} handleSLUpdate={(locations) => {
            _.set(stories, [k,'locations'], locations);
            this.props.handleStoryUpdate(stories);
          }} /></Form.Item>
        </Form.Item>
        <Icon
          className="dynamic-delete-button"
          type="minus-circle-o"
          disabled={keys.length === 1}
          onClick={() => this.remove(k)}
        />
      </div>
    ));
    return (
      <Form>
        {formItems}
        <Form.Item className="StoryForm__wrapper">
          <Empty
            description={
                <span>
                  Stories represent a location or a series of location in a day!
                </span>
            }>
          </Empty>
          <Button type="primary" onClick={this.add} className="StoryForm__button">Add a new story</Button>
        </Form.Item>
      </Form>
    );
  }
}

const WrappedStory = Form.create({ name: 'dynamic_story_location', onValuesChange: (props, changedValues, allValues) => {
  _.each(allValues, function(value, key) {
    if(key !== 'keys'){
      const temp = key.split('-');
      if(temp.length === 3){
        // temp = [story, index, name]
        _.set(stories, [temp[1],temp[2]], value);
      }
    }
  });
  props.handleStoryUpdate(stories)}
 })(Story);
export default WrappedStory;
