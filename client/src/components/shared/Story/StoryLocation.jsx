import React, {Component} from 'react';
import { Form, Input, Icon, Button, Rate} from 'antd';
import _ from 'lodash';
import Picture from '../Picture.jsx';
import Location from './Location';
import './StoryLocation.css';

let id=0;

class StoryLocation extends Component {
  remove = (k) => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys,
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const { storyID } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 24 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      },
    };
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 20, offset: 0 },
      },
    };
    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => (
      <div className="StoryLocation" key={`${storyID}-location-${index}`}>
        <Form.Item
          label='Check in at'
          required={false}
          key={k}
          className="StoryLocation__form"
          name={`${storyID}-location-${index}-name`}
        >
          {getFieldDecorator(`${storyID}-location-${index}-name`, {
            validateTrigger: ['onChange', 'onBlur'],
            rules: [{
              required: true,
              whitespace: true,
              message: "Need that location to check in!",
            }],
          })(
            <Location />
          // <Input placeholder="Location name" style={{ width: '60%', marginRight: 8 }} />
          )}
          <Form.Item name={`${storyID}-location-${index}-picture`}>
          {getFieldDecorator(`${storyID}-location-${index}-picture`)(
            <Picture />
          )}
          </Form.Item>
          
          <Form.Item extra="Rate this location to help us understand your experience better!" name={`${storyID}-location-${index}-rate`}>
          {getFieldDecorator(`${storyID}-location-${index}-rate`, {
              initialValue: 4,
              })(
            <Rate />
          )}
          </Form.Item>
        </Form.Item>
        <Icon
          className="dynamic-delete-button"
          type="minus-circle-o"
          disabled={keys.length === 1}
          onClick={() => this.remove(k)}
        />
      </div>
    ));
    return (
      <Form onSubmit={this.handleSubmit}>
        {formItems}
        <Form.Item {...formItemLayoutWithOutLabel}>
          <Button type="dashed" shape="round" icon="plus" onClick={this.add} >
            Add a new location
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const WrappedStoryLocation = Form.create({ name: 'dynamic_story_location', onValuesChange: (props, changedValues, allValues) => {
  let locations = [];
  _.each(allValues, function(value, key) {
    if(key !== 'keys'){
      const temp = key.split('-');
      if(temp.length === 5){
        // temp = [story, index, location, index, name]
        _.set(locations, [temp[3],temp[4]], value);
      }
    }
  });
  console.log("what are locations", locations);
  props.handleSLUpdate(locations); 
} })(StoryLocation);
export default WrappedStoryLocation;
