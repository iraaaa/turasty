import React, {Component} from 'react';
import './Location.css';

export default class Location extends Component{
  render(){
    return (
      <div className={`Location__thumbnail__box ${this.props.className}`}>
        <i className="fa fa-map-marker" aria-hidden="true"></i>
        <span className="Location__thumbnail__primary">{this.props.location.primary}</span>
        <div className="Location__thumbnail__sub">
        {this.props.location.subLocations.map((subLocation,i) => {
          return (<span className="Location__thumbnail__sub__item" key={`location__${i}`}>{subLocation}</span>)
        })}
        </div>
      </div>
    )
  }
}
