import React, {Component} from 'react';
import './Author.css';
import img from './../img/image-placeholder.jpg';
/**
props
className
author :{
 username : String,
 href : String
 imgSrc : String
}
**/

export default class Author extends Component{
  render(){
    return (
      <a className={`Author__thumbnail__box ${this.props.className}`}
          href={this.props.author.href} >
        <img className="Author__thumbnail__dp" src={img} alt=""/>
        <span className="Author__thumbnail__name">{this.props.author.username}</span>
      </a>
    )
  }
}
