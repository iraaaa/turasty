import React, {Component} from 'react';
import './Trip.css';
import img from './../img/image-placeholder.jpg';
import Author from './../Author/Author.jsx';
import Icon from './../Icon/Icon.jsx';
import Location from './../Location/Location.jsx';
import Tag from './../../Tag/Tag.jsx';

export default class Trip extends Component{

  render(){
    return(
      <div className={this.props.className}>
        <div className="TripThumbnail__item
                        TripThumbnail__header">
          <Author className="TripThumbnail__header__item
                            TripThumbnail__header__author"
                   author = {this.props.data.author}
          />
          <span className="TripThumbnail__header__item
                           TripThumbnail__header__timestamp">
            posted {this.props.data.timeStamp} ago
          </span>


          <div className="TripThumbnail__header__item
                          TripThumbnail__header__dataIcons">
          {this.props.data.dataIcons.map((icon,i)=>{
            return (<Icon key={`icon__${i}`} className="TripThumbnail__header__dataIcons__item" icon={icon} />)
          })}
          </div>
        </div>
        <img className="TripThumbnail__item
                        TripThumbnail__cover" src={img} alt="Not available"/>
        <div className="TripThumbnail__item
                        TripThumbnail__desc">
          <span className="TripThumbnail__desc__item
                           TripThumbnail__desc__heading">{this.props.data.heading}</span>

          <Location className="TripThumbnail__desc__item
                          TripThumbnail__desc__location"
                    location={this.props.data.location}
          />

          <div className="TripThumbnail__desc__item
                          TripThumbnail__desc__tags">
           {this.props.data.tags.map((tag,i) =>{
            return (<Tag tag={tag} key={`tag__${i}`} />)
          })}
          </div>
        </div>
      </div>
    )
  }
}
