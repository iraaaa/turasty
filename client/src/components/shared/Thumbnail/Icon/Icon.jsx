import React, {Component} from 'react';
import './Icon.css';

export default class Icon extends Component{
  render(){
    return (
      <div className={`Icon__thumbnail__box ${this.props.className}`}>
        <i className={`${this.props.icon.className} Icon__thumbnail__icon`} aria-hidden="true"></i>
        <span className="Icon__thumbnail__data">{this.props.icon.data}</span>
      </div>
    )
  }
}
