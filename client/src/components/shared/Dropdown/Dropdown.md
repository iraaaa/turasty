# Dropdown design

+ To design a dropdown which can have the following functionalities:
  1. Simple Dropdown which takes in data and displays options. Items can be searched from the list. Only one item can be selected from the list. one item can be selected from the list.
  1. Multiselect Dropdown where user can select more than one item from the list.

+ Basic Markup needed:
  1. Input element
  2. Button to toggle data display
  3. div/ul to encompass results
  4. data items as li; if empty display "no results available"
+ Markup for Multiselect
  1. div/box/button/checkbox inside each li item to highlight selected item
  2. div with 'x' button to display/cancel selected data in input box

+ Props needed:
  1. data: [] ; array of string values !required

+ State variables needed:
  1. showResults : true|| false ; toggle data display
  2. data: [] ; initial data received from props
  3. results: [] ; data that populates the li items
  4. selected : [] ; array to store the selected values and display them in the input box
  5. multiple : true || false ; toggle multiple select or simple dropdown

+ Behavior needed:
  1. handleChange: to handle input focus and defocus. On input focus, set showResults true. Also, as the user types show matched results in the list. If no match, return empty results array.
  2. handleClick: to handle selecting li item.
    + If multiple is false, push selected item into the selected state array, set showResults to false and change input text to selected element text.
    + If multiple is true, push selected item into the selected state array, add selected element text to input div.
