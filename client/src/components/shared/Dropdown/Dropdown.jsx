import React, {Component} from 'react';
import './Dropdown.css';
//data: [], multiple: boolean, placeholder
// state
// selected has index of selected items from results box
export default class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showResults: false,
      initialData: [],
      results: [],
      selected: [],
      text: '',
    }
    if(props.data){
      props.data.map((item,i)=>{
        this.state.initialData.push({index:i , data:item});
        this.state.results.push({index:i , data:item});
        return true;
      });
    }
    this.handleChange = this.handleChange.bind(this);
    this.toggleData = this.toggleData.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.removeSelected = this.removeSelected.bind(this);
    this.handleCx = this.handleCx.bind(this);
  }
  handleChange(e){
    let text = e.target.value;
    this.setState((prevState)=>{
      return {
        text: text,
        showResults: true,
        results: prevState.initialData.filter((item)=>{
          return item.data.toString().toLowerCase().indexOf(text.toLowerCase()) > -1
        })
      }
    });
  }
  toggleData(){
    this.setState((prevState) => {
      return {showResults:!prevState.showResults};
    });
  }
  handleBlur(e){
    this.setState({showResults:false});
  }
  handleFocus(e){
    this.setState({showResults:true});
  }
  handleClick(e){
    let index,tag;
    tag = e.target.tagName!== 'LI' ? e.target.parentNode : e.target;
    index = tag.id.split("-").pop();

    if(this.props.multiple){
      let i = this.state.selected.indexOf(index);
      if(i<=-1){
        //it does not exist already
        this.setState((prevState)=>prevState.selected.push(index));
        //give bkg className
      }else{
        //it exists, then remove from selected
        this.setState((prevState)=>prevState.selected.splice(i,1));
      }
    }else{
      this.setState({
        text:this.state.initialData[index].data,
        showResults: false
      });
    }
  }
  removeSelected(e){
    e.preventDefault();
    let index = (e.target.tagName!== 'DIV') ?
                e.target.parentNode.id.split("-").pop()
                :
                e.target.id.split("-").pop();
    this.setState((prevState)=>{
      let i = prevState.selected.indexOf(index);
      return prevState.selected.splice(i,1);
    });
  }
  handleCx(index){
    return (this.state.selected.indexOf(index.toString())>-1)? true : false;
  }
  render(){
    return(
      <div className={`Dropdown ${this.props.className}`}>
        <div className="Dropdown__query">
          {
            this.state.selected.length ?
            ( <div className="Dropdown__query__item Dropdown__query__selected">
              {this.state.selected.map((index) =>
                (<div id={`selected-${index}`} key={`selected_${index}`} className="Dropdown__query__item__selected-item" onClick={this.removeSelected}>
                  <span>{(this.state.initialData[index])? this.state.initialData[index].data  : null}</span>
                  <i className="fa fa-times Dropdown__query__item__selected-item__cancel"></i>
                 </div>
                ))
              }
              </div>
            )
            : null
          }
          <div className="Dropdown__query__item">
            <input type="text"
                   className="Dropdown__query__item__input"
                   onChange = {this.handleChange}
                   onFocus = {this.handleFocus}
                   placeholder = {this.props.placeholder}
                   value = {this.state.text}
             />
             <button className="Dropdown__query__item__btn" onClick={this.toggleData}>
              {this.state.showResults ?
              <i className="fa fa-caret-up"></i>
              :
              <i className="fa fa-caret-down"></i>
              }
             </button>
          </div>
        </div>

        {
          this.state.showResults ?
          (<div className="Dropdown__results">
            <ul onMouseDown={this.handleClick}>
              {
                this.state.results.length ?
                ( this.state.results.map((item) =>
                   (
                    <li key={item.index}
                        id={`dropdown_li-${item.index}`}
                        className={(this.handleCx(item.index))?"Dropdown__results__elem--selected":"Dropdown__results__elem"}
                    >
                      <span>{item.data}</span>
                    </li>
                   )
                )):
                (<li className="Dropdown__results__elem"><span>No results available </span></li>)
              }
            </ul>
          </div>)
        : null
      }
      </div>
    )
  }
}
