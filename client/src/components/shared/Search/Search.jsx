import React, {Component} from 'react';
import './Search.css';
import Dropdown from '../Dropdown/Dropdown.jsx';

export default class Search extends Component{
  constructor(props){
    super(props);
    this.state = {
      showResults: false,
      results : [],
      showFilters: false,
    }
    this.handleFilters = this.handleFilters.bind(this);
  }
  handleFilters(){
    this.setState({showFilters: !this.state.showFilters})
  }
  render(){
    return(
      <div className="Search__container">
        <div className="Search__box">
          <div className="Search__box__element Search__box__element--wide">
            <input type="text" placeholder="Search places"
                    className="Search__box__element__child
                              Search__box__element__child--wide
                              Search__box__input" />
            <button className="Search__box__element__child
                              Search__box__element__child--thin
                              Search__box__input__enterBtn">
              <i className="fa fa-search" aria-hidden="true"></i>
            </button>
          </div>
          <button className="Search__box__element
                             Search__box__element--thin
                             Search__box__addFiltersBtn"
                  onClick={this.handleFilters}>
            <i className="fa fa-sliders fa-3x" aria-hidden="true"></i>
          </button>
        </div>
        {(this.state.showResults) ?
        <div className="Search__results__box"></div>
        : null }
        {(this.state.showFilters) ?
        <div className="Search__filters__box">

          <div className="Search__filters__box__element
                          Search__filters__box__duration">
            <div className="Search__filters__box__element__child
                            Search__filters__box__element__child__label">
              Duration
            </div>
            <input type="text" placeholder="Enter number"
                  className="Search__filters__box__element__child
                            Search__filters__box__duration__input"
            />
            <Dropdown placeholder="Number"
                      multi={false} data={["days","weeks","months","years"]} />

          </div>

          <div className="Search__filters__box__element
                          Search__filters__box__budget">
            <div className="Search__filters__box__element__child
                            Search__filters__box__element__child__label">
              Budget
            </div>
            <input type="text" placeholder="min"
                  className="Search__filters__box__element__child
                             Search__filters__box__budget__min"
            />
            <input type="text" placeholder="max"
                  className="Search__filters__box__element__child
                             Search__filters__box__budget__max"  />

          </div>

          <div className="Search__filters__box__element
                          Search__filters__box__seasons">
            <div className="Search__filters__box__element__child
                            Search__filters__box__element__child__label">
            Seasons
            </div>
            <Dropdown placeholder="Seasons"
                      multiple={true} data={["spring","fall","summer","winter","monsoon"]} />
          </div>

          <div className="Search__filters__box__element
                          Search__filters__box__tags">
            <div className="Search__filters__box__element__child
                            Search__filters__box__element__child__label">
            Tags
            </div>
            <Dropdown placeholder="Tags"
                      multiple={true} data={["adventure","relax","kayaking","fun"]} />
          </div>
        </div>
        :null }
      </div>


    )
  }
}
