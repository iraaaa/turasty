import React, { Component } from 'react';
import './Nav.css';
import img from './img/image-placeholder.jpg';
import { NavLink, withRouter } from 'react-router-dom';
import auth from '../../../Auth';

class Nav extends Component{
  // constructor(props){
  //   super(props);
  // }

  logout = () => {
    auth.logout();
    this.props.history.replace('/');
  };

  render(){
    return (
      <nav className="Nav">
        <div className="Nav__brand Nav__brand--box">
          <img className="Nav__brand__img" src={img} alt=""/>
        </div>
        <div className="Nav__items Nav__items--box">
          <NavLink to="/" className="Nav__items__item Nav__items__item--box" >
            <i className="fa fa-home Nav__items__item__icons" aria-hidden="true"></i>
            <span className="Nav__items__item__label" >Home</span>
          </NavLink>
          {
              ( auth.isAuthenticated() ) ? 
              <NavLink to="/profile" className="Nav__items__item Nav__items__item--box">
                <i className="fa fa-compass Nav__items__item__icons" aria-hidden="true"></i>
                <span className="Nav__items__item__label" >Profile</span>
              </NavLink>
            : ''
          }
          {
              ( auth.isAuthenticated() ) ? 
              <NavLink to="/create" className="Nav__items__item Nav__items__item--box">
                <i className="fa fa-plus Nav__items__item__icons" aria-hidden="true"></i>
                <span className="Nav__items__item__label" >Create a Trip</span>
              </NavLink>
            : ''
          }
        </div>
        {
          (auth.isAuthenticated()) ? 
          (<button className="Nav__buttons__btn Nav__buttons__btn--colored Nav__buttons__btn--box" onClick={() => this.logout()}>Log out</button>)
          :
          (<span className="Nav__buttons__btn Nav__buttons__btn--colored Nav__buttons__btn--box login" onClick={() => auth.login()}>Login | Join</span>)
        }
        {/* <NavLink to="/authenticate" className="Nav__buttons Nav__buttons--box">
          <span className="Nav__buttons__btn Nav__buttons__btn--colored Nav__buttons__btn--box">Login | Join</span>
        </NavLink> */}
      </nav>
    )
  }
}

export default withRouter(Nav);