import React, {Component} from 'react';

// props: width, height color

export default class Circle extends Component{
  render(){
    return(
      <div
      style={{
        width: '0px',
        height: this.props.height ? this.props.height : '3px',
        color: this.props.color ? this.props.color : 'black',
        borderStyle : this.props.dotted? `dotted` : `solid`,
        borderWidth: this.props.width ? this.props.width : '1px'
       }}
       className={this.props.className}
       >
       </div>
    )
  }
}
