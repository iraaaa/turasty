import React, {Component} from 'react';
import './GridList.css'

export default class GridList extends Component{
  constructor(props){
    super(props);
    this.addDefaultClassName = this.addDefaultClassName.bind(this);
  }
  /**
  this method returns clones of the children with the added className of GridList__box__element
  this ensures we are forcing our grid styles on each children without making the component tightly bound
  **/
  addDefaultClassName(child){
    if(child){
      return React.cloneElement(child,{
        className: `GridList__box__element ${child.props.className}`
      });
    }
    return null;
  }
  render(){
    return(
      <div className={`GridList__box ${this.props.className}`}>
        {React.Children.map(this.props.children,this.addDefaultClassName)}
      </div>
    )
  }
}
