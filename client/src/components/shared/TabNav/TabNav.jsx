import React, {Component} from 'react';
import './TabNav.css'
import TabList from './TabList/TabList.jsx';


export default class TabNav extends Component{
  constructor(props){
    super(props);
    this.state= {
      selected : 0,
    }
    this.handleTabNavClick = this.handleTabNavClick.bind(this);
  }
  handleTabNavClick(index){
    this.setState({selected:index});
  }
  render(){
    return (
      <TabList className="TabNav__tabList__box" data={this.props.data} selected={this.state.selected} clickFn={this.handleTabNavClick} />
    )
  }

}
