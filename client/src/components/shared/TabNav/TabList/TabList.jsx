import React, {Component} from 'react';
import './TabList.css';
import { Link } from 'react-router-dom';
//data => [{name,href}], clickFn => handler to clicking on each link, selected = index of selected tab
export default class TabList extends Component{
  constructor(props){
    super(props);
    this.state = {
      selected : this.props.selected,
    }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps){
       this.setState((prevState, nextProps) => ({
         selected : nextProps.selected
       }));
    }
  }
  render(){
    return(
      <div className={this.props.className}>
      {
        this.props.data.map((tab,i) => {
          let index = parseInt(this.state.selected,10);
          if(i===index){
            return (<Link to={tab.href} className="TabList__tab TabList__tab--active" href={tab.href} onClick={this.props.clickFn.bind(this,i)} key={`tab__${i}`}>
              {tab.name}
              <div className="TabList__tab--dot"></div>
              </Link>)
            }else
            return (<Link to={tab.href} className="TabList__tab" href={tab.href} onClick={(this.props.clickFn)? this.props.clickFn.bind(this,i) : null} key={`tab__${i}`}>{tab.name}</Link>)
        })
      }
      </div>
    )
  }
}
