import React, {Component} from 'react';

import TabNav from '../TabNav/TabNav.jsx';
import GridList from '../List/GridList/GridList.jsx';
import TripThumbnail from '../Thumbnail/Trip/Trip.jsx';

export default class GridTabCombo extends Component{

  render(){
    return (
      <div>
        <TabNav data={this.props.tabData} />
        <GridList>
        { this.props.tripData.map((trip,i) => {
          return (
            <TripThumbnail className={this.props.className} key={`trip__${i}`} data={trip} />
          )}
        )}
        </GridList>
      </div>
    )
  }
}
