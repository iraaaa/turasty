import React, {Component} from 'react';
import { Query } from "react-apollo";
import { gql } from "apollo-boost";

import TabNav from '../TabNav/TabNav.jsx';
import GridList from '../List/GridList/GridList.jsx';
import TripThumbnail from '../Thumbnail/Trip/Trip.jsx';

export default class GridTabCombo extends Component{
  getTripQuery(username) {
    console.log("is username ok", username);
    return gql`
      query tripsByUsername {
        tripsByUsername(username:"${username}"){
          author {
            bio,
            countries,
            published,
          },
          cover,
          title,
          tags,
          stories
        }
      }
    `;
  }

  render(){
    const TRIP_QUERY = this.getTripQuery(this.props.username);
    console.log("whats teh query", TRIP_QUERY);
    return (
      <Query query={TRIP_QUERY}>
        {({ loading, error, data }) => {
          console.log("in trip," , data);
          return (
            <div>
              <TabNav data={this.props.tabData} />
              <GridList>
              { this.props.tripData && this.props.tripData.map((trip,i) => {
                return (
                  <TripThumbnail className={this.props.className} key={`trip__${i}`} data={data} />
                )}
              )}
              { data && data.trips && data.trips.map((trip,i) => {
                return (
                  <TripThumbnail className={this.props.className} key={`trip__${i}`} data={data} />
                )}
              )}
              </GridList>
            </div>
          );
        }}
      </Query>
      
    )
  }
}
