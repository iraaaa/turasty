import React, {Component} from 'react';
import './Tag.css';

const Tag = ({tag}) => (
  <a href={tag.href} className="Tag__box">
    <i className="fa fa-tag Tag__icon" aria-hidden="true"></i>
    <span className="Tag__label">{tag.label}</span>
  </a>
 );
 
 export default Tag;