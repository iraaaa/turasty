import React, { Component } from 'react';
import './PlaceSearch.css';

export default class Search extends Component{
  constructor(props){
    super(props);
    this.state = {
      text : '',
      results : [],
      showResults:false,
      focusItemIndex : null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.changeFocus = this.changeFocus.bind(this);
    this.keyEvents = this.keyEvents.bind(this);
    this.mouseEvents = this.mouseEvents.bind(this);
    this.acceptValue = this.acceptValue.bind(this);
    this.resetState = this.resetState.bind(this);
    this.hideResults = this.hideResults.bind(this);
  }
  handleChange(e){
    let value = e.target.value;
    this.setState({text:value});

    //make api calls when input length is > 3 for better results
    if(value.length>=3){
      let query = `query SearchPlaces($input: String!){
        searchPlaces(text: $input) {
          features {
            place_name
          }
        }
      }`
      let vars  = {input: value}
      let opts = {}
      opts.body = JSON.stringify({
        query: query,
        variables: vars
      });
      opts.method = "POST";
      opts.headers =  new Headers();
      opts.headers.append('content-type', 'application/json');

      console.log(opts);

      fetch("/graphql/",opts)
      .then((response) => response.json())
      .then((response)=>{
        if(response && response.data.searchPlaces.features.length>0){
          this.setState({results:response.data.searchPlaces.features,showResults:true})
        }else{
          this.setState({results:[{place_name:"No results found"}],showResults:true})
        }
      }).catch((err) =>{
        this.setState({results:[{place_name:"No results found"}],showResults:true})
      });
    }
  }
  changeFocus(direction){
    if(direction){
      this.setState((prevState) => {
        let index =  prevState.focusItemIndex === null? 0 : (parseInt(prevState.focusItemIndex,10) + parseInt(direction,10)),
            len = this.state.results.length-1;
        index =  Math.max(0,Math.min(len,index));
        return {focusItemIndex: index}
      })
    }
  }
  keyEvents(e){
    switch (e.which) {
      //up
      case 38:
        e.preventDefault();
        e.stopPropagation();
        this.changeFocus(-1);
        break;
      //down
      case 40:
        e.preventDefault();
        e.stopPropagation();
        this.changeFocus(1);
        break;
      //backspace
      case 8:
        if(this.state.text.length<=3){
          this.hideResults()
        }else{
          this.setState({focusItemIndex:null})
        }
        break;
      //tab
      case 9:
        this.acceptValue();
        break;
      //delete clear esc
      case 46:
      case 12:
      case 27:
        this.resetState();
        break;
      //enter
      case 13:
        e.preventDefault();
        this.acceptValue();
        break;
      default:
        break;
    }
  }
  mouseEvents(e){
    let x = e.target;
    //parseInt to avoid type mismatch
    this.setState({focusItemIndex:parseInt(x.id,10)});

  }
  acceptValue(x){
    if(this.state.focusItemIndex !== null){
      this.setState({text:this.state.results[this.state.focusItemIndex].place_name});
      this.hideResults()
    }
  }
  resetState(){
      this.setState({text : '',results : [],showResults:false,focusItemIndex : null});
  }
  hideResults(){
    this.setState({results:[],showResults:false,focusItemIndex:null});
  }
  render(){
    return (
      <div className = "container">
        <div className = "inputContainer">
          <button className = "atSymbol">
            <i className ="fa fa-location-arrow" aria-hidden="true"></i>
          </button>
          <div className= "styles.input">
            <input
             type="text"
             value={this.state.text}
             onChange={this.handleChange}
             onKeyDown={this.keyEvents}
             onBlur = {this.hideResults}
             placeholder = "Search Places"/>
          </div>
          <button className="styles.cancel" onClick={this.resetState}>
            <i className ="fa fa-times" aria-hidden="true"></i>
          </button>
        </div>
        {this.state.showResults ?
        <div className = "resultsContainer">
          <div className= "results">
            <div className = "fetched">
              <ul className = "ul">
                {this.state.results.map((place,i) =>
                  <li
                  id = {i}
                  key={place.place_name}
                  onMouseEnter={this.mouseEvents}
                  onMouseDown={this.acceptValue}
                  className={`${this.state.focusItemIndex === i ? 'active' : ''}`}
                  >
                  {place.place_name}
                  </li>
                )
              }
              </ul>
            </div>
          </div>
        </div>
       : null }
      </div>
    )
  }
}
