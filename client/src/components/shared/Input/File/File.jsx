import React, {Component} from 'react';
import generateID from '../../../../utils/genID.js';
import './File.css';

export default class Input extends Component{
  constructor(props){
    super(props);
    this.state = {
      id : '',
    }
  }
  componentWillMount(){
    this.setState({id:generateID("input__")});
  }
  render(){
    return (
      <div className={this.props.className}>
        <input type="file"
               id={this.state.id}
               onChange={this.props.handleChangeFn}
               accept={this.props.accept}
               className="File__input" />
        <label htmlFor={this.state.id} className="File__input__label">
          {this.props.children}
        </label>
      </div>
    )
  }
}
/**
className
handleChangeFn : handle change of input
accept :type of files
cannot have button as child because it interferes with the click event
**/
