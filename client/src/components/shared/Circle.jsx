import React, {Component} from 'react';

// props: size, color , children

export default class Circle extends Component{
  render(){
    return(
      <div
      style={{
        borderRadius:'50%',
        width: this.props.size ? this.props.size : '1rem',
        height: this.props.size ? this.props.size : '1rem',
        backgroundColor: this.props.color ? this.props.color : 'black'
       }}
       className={this.props.className}
       >
        {this.props.children}
       </div>
    )
  }
}
