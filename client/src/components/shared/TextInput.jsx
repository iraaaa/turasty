import React, {Component} from 'react';
// props: className, labelClass, text, placeholder, fn
export default class TextInput extends Component{
  render(){
    return(
      <div className={this.props.className}>
        <span className={this.props.labelClass}>{this.props.text}</span>
        <input type="text" placeholder={this.props.placeholder} onChange={this.props.fn}/>
      </div>
    )
  }

}
