import React, {Component} from 'react';
import './Location.css';
import '../../../../utils/utils.css';
import Input from '../../TextInput.jsx';
import Line from '../../Line.jsx';
import Grid from '../../List/GridList/GridList.jsx';
import Upload from '../../Input/File/File.jsx';

export default class Location extends Component{
  render(){
    return(
      <div className="Location-edit__box">
        <div className="Location-edit__header">
          <i className="fa fa-map-marker icon--med" aria-hidden="true"></i>
          <Line height="2rem" />
        </div>
        <div className="Location-edit__body">
          <section className="Location-edit__body__location">
            <Input className="Location-edit__body__location__elem" labelClass="" text="Check in at" placeholder=""  />
            <Input className="Location-edit__body__location__elem" labelClass="" text="Coming From(optional)" placeholder=""  />
          </section>
          <section className="">
            <Grid className="Location-edit__imageGrid">
              <div className="Location-edit__imageGrid__elem">
                <Upload className="flex-push--center">
                  <i className="fa fa-plus-circle icon--lg" aria-hidden="true"></i>
                  <p>Add images</p>
                </Upload>
              </div>
            </Grid>
          </section>
          <section className="Location-edit__otherInfo">


          </section>
          <section className="Location-edit__btnGrp"></section>
        </div>
      </div>
    )
  }
}
