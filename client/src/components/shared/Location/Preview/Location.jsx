import React, {Component} from 'react';
import './Location.css';
import '../../../../utils/utils.css';
import Circle from '../../Circle.jsx';
import Line from '../../Line.jsx';
import GridList from '../../List/GridList/GridList.jsx';
/**
index: 1,
at: "Banff National Park",
from: {location: "San Francisco", new: true},
img: [],
transport: {type:"car",icon:"fa fa-car"},
feeling: "amazing",
budget: {val:2500, currency:"USD"},
notes: "",

**/


export default class LocationPreview extends Component{
  render(){
    let data = this.props.location;
    if(data){
    console.log("we have data");
    return (
      <div className="flex-col--wrap Location__box">
      {(data.from.new)?
       (
        <section className="flex-row--nowrap flex-align--end">
          <div className="Location__sec-elem--narrow"></div>
          <div className="flex-col--nowrap Location__sec-elem--med ">
            <Line className="flex-push--center" height="1.3rem" dotted="true"/>
            <Circle className="flex-push--center" />
          </div>
          <div className="Location__sec-elem--wide color--blue">
            {data.from.location}
          </div>
        </section>
       ):null
      }
        <section className="flex-row--nowrap Location__sec-transport">
          <div className="flex-self--center ta-center Location__sec-elem--narrow color--dark-gray">
            {
              data.transport.icon ?
            (<i className={`${data.transport.icon}`}></i>) : null
            }
          </div>
          <div className="flex-col--nowrap Location__sec-elem--med">
            <Line className="flex-push--center flex-spread--equal color--dark-gray"/>
          </div>
          <div className="Location__sec-elem--wide"></div>
        </section>
        <section className="flex-row--nowrap flex-align--center">
          <div className="Location__sec-elem--narrow"></div>
          <div className="Location__sec-elem--med">
            <Circle size="2rem" className="flex-push--center Location__sec-elem__circle">
              <span>{data.index}</span>
            </Circle>
          </div>
          <div className="flex-row--wrap Location__sec-elem--wide">
            <div className="flex-spread--equal ta-center color--blue">{data.at}</div>
            <div className="flex-spread--equal ta-center color--orange">-&nbsp;Feeling&nbsp;{data.feeling}</div>
          </div>
        </section>
        <section className="flex-row--nowrap">
          <div className="Location__sec-elem--narrow"></div>
          <div className="Location__sec-elem--med flex-col--nowrap">
            <Line className="flex-push--center flex-spread--equal" />
          </div>
          <div className="Location__sec-elem--wide">
            <GridList className="Location__sec-elem__grid">
              {data.img.map(elem =>
                <div className="Locaton__sec-elem__data">
                  <img className="" src="" alt="n/a" />
                </div>)}
            </GridList>
          </div>
        </section>
      </div>
    )}
    else
    return null;
  }
}
