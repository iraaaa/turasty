import img from './img/image-placeholder.jpg';

export function userData(isSignedIn){
  if(isSignedIn){
    return {
      username: "abcdef",
      name: "Jane Dowestripla",
      coverImg: {img},
      profileImg: {img},
      info: [
        {label:"published",value:"20"},
        {label:"followers",value:"1k"},
        {label:"following",value:"645"}
      ]
    }
  }
  return null;
}

export function getHomeTabs(isSignedIn){
  if(isSignedIn)
  return [
    {name:"Following",href:"#"},
    {name:"Trending",href:"#"},
    {name:"New",href:"#"}
  ];
  else
  return [
    {name:"Trending",href:"#"},
    {name:"New",href:"#"}
  ];
}
export function getProfileTabs(){
  return [
    {name:"Published",href:"#"},
    {name:"Liked",href:"#"}
  ];
}

export function getAllTrips() {
  return (
    [
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
      {
        author :{
         username : "arimai",
         href : "#",
         imgSrc : ""
        },
        timeStamp : "2h",
        dataIcons : [
          {
            className:"fa fa-heart-o",
            data:"3k"
          },
          {
            className:"fa fa-clock-o",
            data:"1 wk",
          },
          {
            className:"fa fa fa-usd",
            data:"2500"
          }
        ],
        cover : {
          imgSrc : ""
        },
        heading: "City tour in Canada",
        location: {
          primary: "Banff",
          subLocations : ["Vancouver","Toronto","Alberta","Banff"]
        },
        tags : [
          {
            label: "1tag",
            href: "#"
          },
          {
            label: "2tag",
            href:"#"
          },
          {
            label: "3tag",
            href:"#"
          },
          {
            label: "4tag",
            href:"#"
          },
          {
            label: "5tag",
            href:"#"
          },
        ]
      },
    ]
  )//return end
}
