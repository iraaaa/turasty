/**
 * Source : https://auth0.com/blog/develop-modern-apps-with-react-graphql-apollo-and-add-authentication/
 */
import auth0 from 'auth0-js';

class Auth {

  constructor() {
    this.auth0 = new auth0.WebAuth({
      domain: 'turasty-community.auth0.com',
      clientID: 'ks4Ze79dJ18YHp0XbnIKZIXEVbSPn27B',
      redirectUri: 'http://localhost:3000/callback',
      audience: 'https://turasty-community.auth0.com/userinfo',
      responseType: 'token id_token',
      scope: 'openid profile'
    });

    this.authFlag = 'isLoggedIn';
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.silentAuth = this.silentAuth.bind(this);
    this.getProfile = this.getProfile.bind(this);
  }

  login() {
    this.auth0.authorize();
  }

  logout() {
    localStorage.setItem(this.authFlag, JSON.stringify(false));
    this.userProfile = null;
  }

  getIdToken() {
    return this.idToken;
  }

  isAuthenticated() {
    return JSON.parse(localStorage.getItem(this.authFlag));
  }

  setSession(authResult) {
    this.idToken = authResult.idToken;
    localStorage.setItem(this.authFlag, JSON.stringify(true));
  }

  silentAuth() {
    if(this.isAuthenticated()) {
      return new Promise((resolve, reject) => {
        this.auth0.checkSession({}, (err, authResult) => {
          if (err) {
            localStorage.removeItem(this.authFlag);
            return reject(err);
          }
          this.accessToken = authResult.accessToken;
          this.setSession(authResult);
          resolve();
        });
      });
    }
  }

  handleAuthentication() {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (err) return reject(err);
        if (!authResult || !authResult.idToken) {
          return reject(err);
        }
        this.accessToken = authResult.accessToken;
        this.setSession(authResult);
        // unset profile if set
        this.unsetProfile();
        resolve();
      });
    })
  }

  async getProfile(cb) {
    const profile = this.isProfileSet();
    if (profile) {
      cb(undefined,profile);
    } else{
      this.auth0.client.userInfo(this.accessToken, async (err, profile) => {
        if (profile) {
          this.userProfile = profile;
          localStorage.setItem("user", JSON.stringify(profile));
        }
        cb(err, profile);
      });
    }
  }

  isProfileSet(){
    return JSON.parse(localStorage.getItem("user"));
  }
  unsetProfile(){
    localStorage.removeItem("user");
  }

}

const auth = new Auth();

export default auth;