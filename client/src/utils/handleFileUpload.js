export default function(e){
  e.preventDefault();
  let reader = new FileReader();
  let file = e.target.files[0];

  reader.onloadend = () => {
    //do something with state here
  }
  if (file) {
    reader.readAsDataURL(file);
    //do something with file here
  }
}
