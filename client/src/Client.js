import ApolloClient from "apollo-boost";
import auth from './Auth';

async function getClient(){
  await auth.silentAuth();
  return new ApolloClient({
    uri: "http://localhost:3001/graphql",
    request: operation => {
      operation.setContext(context => ({
        headers: {
          ...context.headers,
          authorization: auth.getIdToken(),
        },
      }));
    },
  });
}
export default getClient;